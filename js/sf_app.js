'use-strict';

angular.module('steadfastApp', ['ui.router', 'ui.bootstrap'])
  .config(function($stateProvider, $urlRouterProvider){
    $stateProvider
      .state('app', {
        url: '/',
        views: {
          'header': {
            templateUrl: 'views/header.html',
            controller: 'HeaderController'
          },
          'content': {
            templateUrl: 'views/home.html'//,
            //controller: 'HomeController'
          },
          'footer': {
            templateUrl: 'views/footer.html'
          }
        }
      })
      .state('app.artists', {
        url: 'artists/:id',
        views: {
          'header@': {
            templateUrl: 'views/galleryHeader.html'
          },
          'content@': {
            templateUrl: 'views/gallery.html',
            controller: 'GalleryController'
          }
        }
      })
      .state('app.shop', {
        url: 'shop',
        views: {
          'content@': {
            templateUrl: 'views/shop.html'
          }
        }
      })
      .state('app.social', {
        url: 'social',
        views: {
          'content@': {
            templateUrl: 'views/contact.html'//,
            //controller: 'ContactController'
          }
        }
      });
    $urlRouterProvider.otherwise('/');
  });
