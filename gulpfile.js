var gulp = require('gulp'),
    minifycss = require('gulp-minify-css'),
    jshint = require('gulp-jshint'),
    stylish = require('jshint-stylish'),
    uglify = require('gulp-uglify'),
    usemin = require('gulp-usemin'),
    imagemin = require('gulp-imagemin'),
    rename = require('gulp-rename'),
    concat = require('gulp-concat'),
    notify = require('gulp-notify'),
    cache = require('gulp-cache'),
    changed = require('gulp-changed'),
    rev = require('gulp-rev'),
    browserSync = require('browser-sync'),
    ngannotate = require('gulp-ng-annotate'),
    del = require('del');


gulp.task('jshint', function() {
    return gulp.src('js/*.js')
                .pipe(jshint())
                .pipe(jshint.reporter(stylish));
});

// Clean
gulp.task('clean', function() {
    return del(['dist']);
});

gulp.task('clear-cache', function() {
    return cache.clearAll();
});

// Default task
gulp.task('default', ['clean'], function() {
    gulp.start('usemin','imagemin', 'copyfonts', 'copyviews', 'copycss');
});

var foreach = require('gulp-foreach');

gulp.task('usemin',['jshint'], function () {
    return gulp.src([ './index.html' ])
        .pipe(foreach(function(stream, file) {
            return stream
                .pipe(usemin({
                    css:[minifycss(),rev()],
                    js: [ngannotate(),uglify(),rev()]
                }))
                .pipe(gulp.dest('dist/'));
        }));
});

// Images
gulp.task('imagemin', ['clear-cache'], function() {
  return del(['dist/img']), gulp.src('img/**/*')
    .pipe(cache(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true })))
    .pipe(gulp.dest('./dist/img'))
    .pipe(notify({ message: 'Images task complete' }));
});

gulp.task('copyfonts', ['clean'], function() {
    gulp.src('../bower_components/font-awesome/fonts/**/*.{ttf,woff,eof,svg}*')
        .pipe(gulp.dest('./dist/fonts'));
    gulp.src('../bower_components/bootstrap/dist/fonts/**/*.{ttf,woff,eof,svg}*')
        .pipe(gulp.dest('./dist/fonts'));
    gulp.src('css/berryr/**/*.{eot,svg,woff}*')
        .pipe(gulp.dest('./dist/css/berryr'));
    gulp.src('css/berryr/images/*.gif')
        .pipe(gulp.dest('./dist/css/berryr/images'));
});

gulp.task('copyviews', ['clean'], function(){
  gulp.src('views/*.html')
    .pipe(gulp.dest('./dist/views'));
});

gulp.task('copycss', ['clean'], function(){
  gulp.src('css/sf-styles.css')
    .pipe(gulp.dest('./dist/css'));
});

// Watch
gulp.task('watch', ['browser-sync'], function() {
  // Watch .js files
  gulp.watch('{js/*.js, css/**/*.css,views/*.html}', ['usemin']);
      // Watch image files
  gulp.watch('img/**/*', ['imagemin']);

});

gulp.task('browser-sync', ['default'], function () {
    var files = [
        'views/*.html',
        'css/**/*.css',
        'img/**/*.png',
        'js/*.js',
        'dist/**/*'
    ];

    browserSync.init(files, {
        server: {
            baseDir: "dist",
            index: "index.html"
        },
        // Needed for Cloud9. Usually needs manual reload.
        port: process.env.PORT,
        host: process.env.IP
    });

    // Watch any files in dist/, reload on change
    gulp.watch(['dist/**']).on('change', browserSync.reload);
});
