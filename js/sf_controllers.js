'use-strict';

angular.module('steadfastApp')
  .controller('HeaderController',['$scope','$state', '$stateParams',
    function($scope, $state, $stateParams){
      $scope.isCollapsed = true;
      $scope.stateis = function(curstate){
        return $state.is(curstate);
      };
    }
  ])
  .controller('GalleryController',
    ['$scope','$state','$stateParams','portfolioFactory',
      function($scope, $state, $stateParams, portfolioFactory){
        $scope.isCollapsed = true;

/*
        $scope.stateis = function(curstate){
          console.log($scope.stateis);
          return $state.is(curstate);
        };
*/
       $scope.artist = portfolioFactory.getArtist($stateParams.id);


     }
  ])
;
