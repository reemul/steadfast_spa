'use-strict';

angular.module('steadfastApp')
  .factory('portfolioFactory', function(){

    var Factory = {};
    // Set of Photos
    var artists = [
      { _id: 0,
        name: 'Pete Gilcrease',
        portfolio: [
        {src: 'img/pete/src/01.png', thb: 'img/pete/thb/01.png', desc: 'Japanese style Tiger'},
        {src: 'img/pete/src/02.png', thb: 'img/pete/thb/02.png', desc: 'Flying Skull Cross'},
        {src: 'img/pete/src/03.png', thb: 'img/pete/thb/03.png', desc: 'Time piece with flowers'},
        {src: 'img/pete/src/04.png', thb: 'img/pete/thb/04.png', desc: 'Revelver with rose background'},
        {src: 'img/pete/src/05.png', thb: 'img/pete/thb/05.png', desc: 'Greek God'},
        {src: 'img/pete/src/06.png', thb: 'img/pete/thb/06.png', desc: 'Daisys on waves'},
        {src: 'img/pete/src/07.png', thb: 'img/pete/thb/07.png', desc: 'Paw Print'},
        {src: 'img/pete/src/08.png', thb: 'img/pete/thb/08.png', desc: 'Mario'},
        {src: 'img/pete/src/09.png', thb: 'img/pete/thb/09.png'},
        {src: 'img/pete/src/10.png', thb: 'img/pete/thb/10.png'},
        {src: 'img/pete/src/11.png', thb: 'img/pete/thb/11.png'},
        {src: 'img/pete/src/12.png', thb: 'img/pete/thb/12.png'},
        {src: 'img/pete/src/13.png', thb: 'img/pete/thb/13.png'},
        {src: 'img/pete/src/14.png', thb: 'img/pete/thb/14.png'},
        {src: 'img/pete/src/15.png', thb: 'img/pete/thb/15.png'},
        {src: 'img/pete/src/16.png', thb: 'img/pete/thb/16.png'},
        {src: 'img/pete/src/17.png', thb: 'img/pete/thb/17.png'},
        {src: 'img/pete/src/18.png', thb: 'img/pete/thb/18.png'},
        {src: 'img/pete/src/19.png', thb: 'img/pete/thb/19.png'},
        {src: 'img/pete/src/20.png', thb: 'img/pete/thb/20.png'},
        {src: 'img/pete/src/21.png', thb: 'img/pete/thb/21.png'},
        {src: 'img/pete/src/22.png', thb: 'img/pete/thb/22.png'},
        {src: 'img/pete/src/23.png', thb: 'img/pete/thb/23.png'},
        {src: 'img/pete/src/24.png', thb: 'img/pete/thb/24.png'},
        {src: 'img/pete/src/25.png', thb: 'img/pete/thb/25.png'},
        {src: 'img/pete/src/26.png', thb: 'img/pete/thb/26.png'},
        {src: 'img/pete/src/27.png', thb: 'img/pete/thb/27.png'},
        {src: 'img/pete/src/28.png', thb: 'img/pete/thb/28.png'},
        {src: 'img/pete/src/29.png', thb: 'img/pete/thb/29.png'}
      ]},
      { _id: 1,
        name: 'Noah Chamberlain',
        portfolio: [
        {src: 'img/noah/src/01.png', thb: 'img/noah/thb/01.png', desc: ''},
        {src: 'img/noah/src/02.png', thb: 'img/noah/thb/02.png', desc: ''},
        {src: 'img/noah/src/03.png', thb: 'img/noah/thb/03.png', desc: ''},
        {src: 'img/noah/src/04.png', thb: 'img/noah/thb/04.png', desc: ''},
        {src: 'img/noah/src/05.png', thb: 'img/noah/thb/05.png', desc: ''},
        {src: 'img/noah/src/06.png', thb: 'img/noah/thb/06.png', desc: ''},
        {src: 'img/noah/src/07.png', thb: 'img/noah/thb/07.png', desc: ''},
        //{src: 'img/noah/src/08.png', thb: 'img/noah/thb/08.png', desc: ''},
        {src: 'img/noah/src/09.png', thb: 'img/noah/thb/09.png', desc: ''},
        {src: 'img/noah/src/10.png', thb: 'img/noah/thb/10.png', desc: ''},
        {src: 'img/noah/src/11.png', thb: 'img/noah/thb/11.png', desc: ''},
        {src: 'img/noah/src/12.png', thb: 'img/noah/thb/12.png', desc: ''},
        {src: 'img/noah/src/13.png', thb: 'img/noah/thb/13.png', desc: ''},
        //{src: 'img/noah/src/14.png', thb: 'img/noah/thb/14.png', desc: ''},
        {src: 'img/noah/src/15.png', thb: 'img/noah/thb/15.png', desc: ''},
        {src: 'img/noah/src/16.png', thb: 'img/noah/thb/16.png', desc: ''},
        {src: 'img/noah/src/17.png', thb: 'img/noah/thb/17.png', desc: ''},
        {src: 'img/noah/src/18.png', thb: 'img/noah/thb/18.png', desc: ''},
        {src: 'img/noah/src/19.png', thb: 'img/noah/thb/19.png', desc: ''},
        {src: 'img/noah/src/20.png', thb: 'img/noah/thb/20.png', desc: ''},
        {src: 'img/noah/src/21.png', thb: 'img/noah/thb/21.png', desc: ''},
        {src: 'img/noah/src/22.png', thb: 'img/noah/thb/22.png', desc: ''},
        {src: 'img/noah/src/23.png', thb: 'img/noah/thb/23.png', desc: ''}
      ]},

      { _id: 2,
        name: 'Randie',
        portfolio: [
        {src: 'img/randie/src/01.png', thb: 'img/randie/thb/01.png', desc: ''},
        {src: 'img/randie/src/02.png', thb: 'img/randie/thb/02.png', desc: ''},
        {src: 'img/randie/src/03.png', thb: 'img/randie/thb/03.png', desc: ''},
        {src: 'img/randie/src/04.png', thb: 'img/randie/thb/04.png', desc: ''},
        {src: 'img/randie/src/05.png', thb: 'img/randie/thb/05.png', desc: ''},
        {src: 'img/randie/src/06.png', thb: 'img/randie/thb/06.png', desc: ''},
        {src: 'img/randie/src/07.png', thb: 'img/randie/thb/07.png', desc: ''},
        {src: 'img/randie/src/08.png', thb: 'img/randie/thb/08.png', desc: ''},
        {src: 'img/randie/src/09.png', thb: 'img/randie/thb/09.png', desc: ''},
        {src: 'img/randie/src/10.png', thb: 'img/randie/thb/10.png', desc: ''},
        {src: 'img/randie/src/11.png', thb: 'img/randie/thb/11.png', desc: ''},
        {src: 'img/randie/src/12.png', thb: 'img/randie/thb/12.png', desc: ''},
        {src: 'img/randie/src/13.png', thb: 'img/randie/thb/13.png', desc: ''},
        {src: 'img/randie/src/14.png', thb: 'img/randie/thb/14.png', desc: ''},
        {src: 'img/randie/src/15.png', thb: 'img/randie/thb/15.png', desc: ''},
        {src: 'img/randie/src/16.png', thb: 'img/randie/thb/16.png', desc: ''},
        {src: 'img/randie/src/17.png', thb: 'img/randie/thb/17.png', desc: ''},
        {src: 'img/randie/src/18.png', thb: 'img/randie/thb/18.png', desc: ''},
        {src: 'img/randie/src/19.png', thb: 'img/randie/thb/19.png', desc: ''},
        {src: 'img/randie/src/20.png', thb: 'img/randie/thb/20.png', desc: ''},
        {src: 'img/randie/src/21.png', thb: 'img/randie/thb/21.png', desc: ''},
        {src: 'img/randie/src/22.png', thb: 'img/randie/thb/22.png', desc: ''},
        {src: 'img/randie/src/23.png', thb: 'img/randie/thb/23.png', desc: ''},
        {src: 'img/randie/src/24.png', thb: 'img/randie/thb/24.png', desc: ''},
        {src: 'img/randie/src/25.png', thb: 'img/randie/thb/25.png', desc: ''},
        {src: 'img/randie/src/26.png', thb: 'img/randie/thb/26.png', desc: ''},
        {src: 'img/randie/src/27.png', thb: 'img/randie/thb/27.png', desc: ''},
        {src: 'img/randie/src/28.png', thb: 'img/randie/thb/28.png', desc: ''},
        {src: 'img/randie/src/29.png', thb: 'img/randie/thb/29.png', desc: ''},
        {src: 'img/randie/src/30.png', thb: 'img/randie/thb/30.png', desc: ''},
        {src: 'img/randie/src/31.png', thb: 'img/randie/thb/31.png', desc: ''},
        {src: 'img/randie/src/32.png', thb: 'img/randie/thb/32.png', desc: ''}
      ]},

    ];
    Factory.getArtist = function(index){
      return artists[index];
    };
    return Factory;
  });
